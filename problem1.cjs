const fs = require('fs');

const path=require('path')

function problem1(){

    const limit=5
    
    try{
        fs.mkdir('./sample', (err) => {
            if (err) {
                console.error(err);
            }
            else{
                console.log('Directory created successfully!')
                for(let index=0;index<limit;index++){
                    fs.writeFile(path.join('./sample',`sample${index+1}.json`),'file created successfully',(err)=>{
                        if (err) {
                            console.error(err);
                        }
                        else{
                            console.log("file created successfully")
                            fs.unlink(path.join('./sample',`sample${index+1}.json`),(err)=>{
                                if(err){
                                    console.error(err)
                                }
                                else{
                                    console.log("file deleted successfully")
                                }
                            })
                                
                        }
                    })
                }
            }
        })
    }
    catch(error){
        console.error(error)
    }
}
module.exports=problem1
