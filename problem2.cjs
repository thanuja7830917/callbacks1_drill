const fs = require('fs')

const path = require('path')

function problem2() {
    try {
        fs.readFile(path.join('../lipsum.txt'), 'utf-8', function (err, data) {
            if (err) {
                console.error(err)
            }
            else {
                console.log("file read successfully")
                const upper = data.toUpperCase()

                fs.writeFile(path.join('upper.txt',), upper, (err) => {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        console.log("the uppercase content written to upper.txt")
                        fs.appendFile("filenames.txt", `upper.txt\n`, (err) => {
                            if (err) {
                                console.error(err);
                            }
                            else {
                                console.log("append successfully")
                                fs.readFile(path.join('upper.txt'), 'utf-8', function (err, data) {
                                    if (err) {
                                        console.error(err)
                                    }
                                    else {
                                        console.log("file upper.txt successfully")

                                        const lower = data.toLowerCase()

                                        const splitted = lower.split('.')

                                        fs.writeFile(path.join('lower.txt',), splitted.toString(), (err) => {
                                            if (err) {
                                                console.error(err)
                                            }
                                            else {
                                                console.log("the lowercase content read successfully")
                                                fs.appendFile("filenames.txt", `lower.txt\n`, (err) => {
                                                    if (err) {
                                                        console.error(err)
                                                    }
                                                    else {
                                                        console.log("append successfully")
                                                        fs.readFile(path.join('lower.txt'), 'utf-8', function (err, data) {
                                                            if (err) {
                                                                console.error(err)
                                                            }
                                                            else {
                                                                const sorted = data.split(',').sort().toString()
                                                                fs.writeFile('sorted.txt', sorted, (err) => {
                                                                    if (err) {
                                                                        console.error(err)
                                                                    }
                                                                    else {
                                                                        console.log("sorted content addes to sorted.txt")
                                                                        console.log("created a file with sorted content")
                                                                        fs.appendFile("filenames.txt", `sorted.txt\n`, (err) => {
                                                                            if (err) {
                                                                                console.error(err)
                                                                            }
                                                                            else {
                                                                                console.log("append successfully")
                                                                                fs.readFile(path.join('filenames.txt'), 'utf-8', function (err, data) {
                                                                                    if (err) {
                                                                                        console.error(err)
                                                                                    }
                                                                                    else{
                                                                                       data.split('\n').forEach((each)=>{
                                                                                            if(each!=""){
                                                                                                fs.unlink(each,(err)=>{
                                                                                                    if(err){
                                                                                                        console.error(err)
                                                                                                    }
                                                                                                    else{
                                                                                                    console.log("file deleted successfully")
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }

                                                                })
                                                            }
                                                        })
                                                    }

                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }

        })
    }
    catch (error) {
        console.error(error)
    }

}
module.exports=problem2

